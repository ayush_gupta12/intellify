import io
import sys
import re

try:
    from PIL import Image
except ImportError:
    import Image
from pytesseract import image_to_string 

# pytesseract.pytesseract.tesseract_cmd = r'<full_path_to_your_tesseract_executable>'



pathImg = sys.argv[1]
imageText = ""


def get_jaccard_sim(str1,str2):
	a = set(str1.split())
	b = set(str2.split())
	c= a.intersection(b)
	return float(len(c))/float((len(a)+len(b)-len(c)))



questionDictionary = {
	"Q1":"What is usually associated with color red",
	"Q2":"What are clothes made of",
	"Q3":"What does it mean if a person is sweet",
	"Q4":"What is sour-graping",
	"Q5":"What does it mean when a thing sells like hotcakes",
	"Q6":"What does it mean if a case investigation runs cold",
	"Q7":"A woodpecker is a kind of",
	"Q8":"The acronym IQ stands for"
} 

def detect_text(path):
    # imageText = "2. What are clothes made of"
    """Detects text in the file."""
    imageText= image_to_string(Image.open(path))
    
    
    print('Text in Image:')
    print(imageText)
    return imageText


if __name__ == '__main__':
    imageText = detect_text(pathImg)
    searchString1=imageText.replace('\n', ' ').replace('\r', '')
    searchString=" ".join(searchString1.split())
    if "?" in searchString:
    	searchString=searchString[:searchString.find("?")]

    print("searchString",searchString)


    mostSimQues_key=""
    maxSimTillNow=(-1.0)

    for key, value in questionDictionary.items():
    	currSimilarity=get_jaccard_sim(searchString,value)
        if(currSimilarity>maxSimTillNow):
            maxSimTillNow= currSimilarity
            mostSimQues_key=key


    print("mostSimQuesNumber",mostSimQues_key)

    print("Question is ", questionDictionary[mostSimQues_key])



