This folder contains two files, one of them uses Google Vision API and the other one does OCR using tesseract API which should be installed locally.  
Google Vision API performs better than tesseract while reading text from images.  
The Python code in both files also suggests most similar question to the one in the image.  
The Python file also needs to be passed as one argument, namely the image that contains the question.   


While Using the Google Vision API one also needs to set a path variable as follows 'export GOOGLE_APPLICATION_CREDENTIALS="apikey.json"'  
