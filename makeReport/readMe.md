The Python Notebook contains code to generate the report shown.  
The 'alldata.csv' also needs to be in the same directory when running the python notebook. This data is belongs to Intellify.  
The cells in python notebook contain a markdown at their start for denoting the work being done in that cell.
The cells have well named variables and comments at necessary places.  
